#################################### Data ######################################
#' ET data set by Hou et al
#'
#' A dataset containing the binarized genotypes of 58 cells of an essential thrombocythemia.
#'
#' @format A matrix with 712 rows and 58 columns, where rows correspond to mutations and columns correspond to cells.
#' @source Originial data from \url{www.sciencedirect.com/science/MiamiMultiMediaURL/1-s2.0-S0092867412002280/1-s2.0-S0092867412002280-mmc2.xls/272196/html/S0092867412002280/02c8fa2fc2602b30b9d4896cbe0b717e/mmc2.xls}, sheet 1.
"Hou"

#' TCC data set by Li et al
#'
#' A dataset containing the binarized genotypes of 44 cells of a muscle-invasive bladder cancer.
#'
#' @format A matrix with 443 rows and 44 columns, where rows correspond to mutations and columns correspond to cells.
#' @source Originial data from \url{http://www.gigasciencejournal.com/content/supplementary/2047-217x-1-12-s5.xls}, sheet 3.
"Li"

#' Reccurenly mutated genes in TCC 
#' 
#' Annotation of genes recurrently mutated in TCC.
#'
#' @format A data frame with 8 rows and 14 columns.
#' @source Originial data from \url{http://www.gigasciencejournal.com/content/supplementary/2047-217x-1-12-s5.xls}, sheet 2.
"Li.recurrGenes"

#' Subpopulations in bladder cells identified by Li et al. 
#' 
#' Annotation of genes recurrently mutated in TCC.
#'
#' @format A data frame with 44 rows and 2 columns.
#' \describe{
#'   \item{BCid}{ID of cell}
#'   \item{cloneid}{ID of the corresponding clone ('A', 'B' or 'C')}
#' }
#' @source Li et al, 2012. Figure 2A.
"Li.annot"

