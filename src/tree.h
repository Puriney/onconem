#ifndef TREE_H
#define TREE_H

#include <cmath>
#include <cstdint>
#include <cstring>
#include <string>
#include <set>
#include <limits>
#include <iostream>

/**
 * Type used for the index of a node.
 *
 * The nodes are enumerated starting from 0 where the root is always
 * the 0 node.
 *
 * An evolution tree is saved as
 *
 *   nodeIndex_t tree[n]
 *
 * where n+1 is the number of nodes. Here tree[i-1] saves the index of
 * the parent for the node with index i.
 */
typedef uint8_t nodeIndex_t;

/**
 * Compares a tree saved as *nodeIndex_t.
 *
 * The length is given as parameter for the constructor.
 */
struct TreeCompare {
  /**
   * Creates a comparison object
   * \param n Number of non-root nodes
   */
  TreeCompare(nodeIndex_t n)
    : n(n)
  {}

  /**
   * Compares two trees
   */
  bool operator()(const nodeIndex_t * x,
		  const nodeIndex_t * y) const
  {
    nodeIndex_t i;
    for (i=0; i < this->n; ++i, ++x, ++y) {
      if (*x < *y) {
	return true;
      } else if (*x > *y) {
	return false;
      }
    }
    return false;
  }

  /**
   * Number of non-root nodes
   */
  nodeIndex_t n;
};

/**
 * Type used to keep a list of all considered trees without scores.
 */
typedef std::set<const nodeIndex_t *, TreeCompare> TreeSet;

/**
 * Type for a scored tree
 */
typedef std::pair<double, const nodeIndex_t *> ScoredTree;

/**
 * Compares a scored tree
 *
 * The length is given as parameter for the constructor. We want high
 * scoring tree to come first.
 */
struct ScoredTreeCompare {
  /**
   * Creates a comparison object
   * \param n Number of non-root nodes
   */
  ScoredTreeCompare(nodeIndex_t n)
    : n(n)
  {}

  /**
   * Compares two trees
   */
  bool operator()(const ScoredTree& x,
		  const ScoredTree& y) const
  {
    if (x.first > y.first) {
      return true;
    } else if (x.first < y.first) {
      return false;
    } else{
      nodeIndex_t i;
      const nodeIndex_t * xt = x.second;
      const nodeIndex_t * yt = y.second;
      for (i=0; i < this->n; ++i, ++xt, ++yt) {
	if (*xt < *yt) {
	  return true;
	} else if (*xt > *yt) {
	  return false;
	}
      }
      return false;
    }
  }

  /**
   * Number of non-root nodes
   */
  nodeIndex_t n;
};

/**
 * Type used to keep a sorted list of all considered trees with scores.
 */
typedef std::set<ScoredTree, ScoredTreeCompare> ScoredTreeSet;

/**
 * Verifies if a graph is valid
 * \param n Number of non-root nodes
 * \param tree Array of predecessors of non-root nodes
 * \return 0 if the tree is valid. Otherwise the index of the invalid
 *         node (where 1 corresponds to the first non-root node
 */
inline unsigned verifyTree(nodeIndex_t n, const nodeIndex_t * tree) {
  for (nodeIndex_t i=0; i < n; ++i) {
    // Check the ancestor tree for node i
    nodeIndex_t pre = tree[i];
    // go up and check the parent, we need to do at most n-1 steps
    for (nodeIndex_t j=0; pre && j < n-1; ++j) {
      if (pre > n) {
	// Error invalid parent
	return i+1;
      }
      pre = tree[pre-1];
    }
    // see if we have not reached the root
    if (pre) {
      return i+1;
    }
  }
  return 0;
}

/**
 * Class representing the influence structure of a tree.
 *
 * This computes and stores the transitive closure and is therefore a
 * quite large object which should not be stored between steps.
 *
 * Since this is an internal class, it does not check the validity of
 * the graph and can result in memory corruption for invalid
 * graphs. It should therefore not be used for external input without
 * validation.
 */
class InfluenceMatrix {
public:
  /**
   * Creates an influence matrix from the nodeIndex_t *
   * representation.
   * \param n Number of non-root nodes
   * \param tree Array of predecessors for non-root nodes
   */
  InfluenceMatrix(nodeIndex_t n, const nodeIndex_t *tree)
    : n(n) // Member initialization in constructor
  {
    // Start by creating the adjacency matrix
    infMatrix = new bool[n*n](); // initialised to zero
#ifdef WARSHALL
    for (unsigned i=0; i<n; ++i) {
      nodeIndex_t parent = tree[i];
      if (parent > 0) {
	(*this)(parent-1,i) = true;
      }
    }
    // Fill in the transitive closure by Warshall's algorithm
    for (unsigned k=0; k<n; ++k) {
      // In each step we make sure that the influence matrix contains
      // all connections over the nodes 1..k
      for (unsigned i=0; i<n; ++i) {
	// i is a possible new parent
	if ((*this)(i,k)) {
	  for (unsigned j=0; j<n; ++j) {
	    // j is a possible new offspring
	    if ((*this)(k,j)) {
	      (*this)(i,j) = true;
	    }
	  }
	}
      }
    }
    // Fill in the diagonal
    for (unsigned i=0; i<n; ++i) {
      (*this)(i,i) = true;
    }
#else
    // Fill in the transitive closure by using the tree structure and
    // going up for every node
    for (unsigned i=0; i<n; ++i) {
      // go up all parents of i
      unsigned pre = tree[i];
      while (pre) {
	(*this)(pre-1,i) = true;
	pre = tree[pre-1];
      }
      // fill in the diagonal
      (*this)(i,i) = true;
    }
#endif
  }
  /**
   * TODO Copy constructor for a safe style
   */
  /**
   * Deconstructs an influence matrix
   */
  ~InfluenceMatrix() {
    delete[] infMatrix;
  }

  /**
   * Accessing arbitrary element.
   * \param i index of parent node - 1 (i.e. counting without root)
   * \param j index of child node - 1 (i.e. counting without root)
   * \return true if node i^th non-root node influences the j^th non-root node.
   */
  inline bool& operator() (unsigned i, unsigned j) {
    return infMatrix[i*n + j];
  }

  /**
   * Accessing arbitrary element, constant version
   * \param i index of parent node - 1 (i.e. counting without root)
   * \param j index of child node - 1 (i.e. counting without root)
   * \return true if node i^th non-root node influences the j^th non-root node.
   */
  inline const bool& operator() (unsigned i, unsigned j) const {
    return infMatrix[i*n + j];
  }

  /**
   * Returns the ith row as C array
   * \param i row
   * \return array with the influences of the i^th non-root node
   */
  inline bool* getRow(unsigned i) {
    return infMatrix + i*n;
  }

  /**
   * Returns the ith row as C array, constant
   * \param i row
   * \return array with the influences of the i^th non-root node
   */
  inline const bool* getRow(unsigned i) const {
    return infMatrix + i*n;
  }

  /**
   * Number of non-root nodes
   */
  const nodeIndex_t n;
 private:
  /**
   * n*n array of influences
   */
  bool * infMatrix;
};

/**
 * Type for indexing the number of outcomes
 */
typedef uint8_t outcomeIndex_t;

/**
 * Class collecting the scoring data
 */
class ScoringData {
public:
  /**
   * Creates a scoring object.
   *
   * The arrays are copied in the construction and can therefore be
   * released afterwards.
   *
   * \param m Number of observations
   * \param n Number of non-root nodes
   * \param numOutcomes Number of outcomes in the measurement
   * \param trueScore[numOutcomes] Score for the outcomes given we predict a mutation
   * \param falseScore[numOutcomes] Score for the outcomes given we predict no mutation
   * \param obsData Observed data, i.e. obsData[i*n + j] gives the measurement
   *        for the i^th SNV at the j^th non-root node
   */
  ScoringData(unsigned m, nodeIndex_t n, outcomeIndex_t numOutcomes,
	      const double * trueScore, const double * falseScore,
	      const outcomeIndex_t * obsData)
    : m(m), n(n), numOutcomes(numOutcomes)
  {
    // Create copies of the data
    this->trueScore = new double[numOutcomes];
    this->falseScore = new double[numOutcomes];
    this->obsData = new outcomeIndex_t[n*m];
    memcpy(this->trueScore, trueScore, (unsigned)numOutcomes * sizeof(double));
    memcpy(this->falseScore, falseScore, (unsigned)numOutcomes * sizeof(double));
    memcpy(this->obsData, obsData, (unsigned)n * m * sizeof(outcomeIndex_t));
  }
  /**
   * TODO Copy constructor
   */
  /**
   * Frees the scoring object
   */
  ~ScoringData() {
    delete [] trueScore;
    delete [] falseScore;
    delete [] obsData;
  }

  unsigned getM() const {
    return m;
  }

  /**
   * Scores a tree given its influence matrix
   * \param infMatrix Influence matrix of the tree to score
   * \return Score of the influence matrix
   */
  double scoreTree(const InfluenceMatrix& infMatrix) const {
    // Sum using Kahan summation to reduce rounding error of the sum
    double sum = 0;
    double sumComp = 0; // Compensation
    for (unsigned i=0; i<m; ++i) {
      // i is going over all SNVs
      const outcomeIndex_t * const mutRow = obsData + i * (unsigned) n;
      // adds the loglikelihood for the ith mutation pattern
      double mutL = 0; // likelihood of this pattern
      for (unsigned j=0; j<n; ++j) {
	// j is the possible place of the true mutation
	double l = 1.;
	const bool * const infRow = infMatrix.getRow(j);
	for (unsigned k=0; k<n; ++k) {
	  bool predMutation = infRow[k];
	  outcomeIndex_t obsOutcome = mutRow[k];
	  if (predMutation) {
	    l *= trueScore[obsOutcome];
	  } else {
	    l *= falseScore[obsOutcome];
	  }
	}
	mutL += l;
      }
      // add log(mutL) with correction
      double y = log(mutL) - sumComp;
      double t = sum + y;
      sumComp = (t - sum) - y;
      sum = t;
    }
    return sum - m * std::log((double)n);
  }

  /**
   * Scores a cluster
   * \param infMatrix Influence matrix of the cluster
   * \param clusterAllocation -> clusterAllocation[i-1] : cluster of cell i
   * TODO Write nicer, explain +- 1 stuff
   * \return Score of the influence matrix
   */
  double scoreCluster(const InfluenceMatrix& infMatrix,
		      const nodeIndex_t * clusterAllocation) const {
    // Sum using Kahan summation to reduce rounding error of the sum
    double sum = 0;
    double sumComp = 0; // Compensation
    for (unsigned i=0; i<m; ++i) {
      // i is going over all SNVs
      const outcomeIndex_t * const mutRow = obsData + i * (unsigned) n;
      // adds the loglikelihood for the ith mutation pattern
      double mutL = 0; // likelihood of this pattern
      for (unsigned j=0; j<infMatrix.n; ++j) {
	// j is the possible place of the true mutation
	double l = 1.;
	const bool * const infRow = infMatrix.getRow(j);
	for (unsigned k=0; k<n; ++k) {
	  nodeIndex_t cluster = clusterAllocation[k];
	  outcomeIndex_t obsOutcome = mutRow[k];
	  if (cluster==0) {
	    l *= falseScore[obsOutcome];
	  } else {
	    bool predMutation = infRow[((int)clusterAllocation[k]-1)];
	    if (predMutation) {
	      l *= trueScore[obsOutcome];
	    } else {
	      l *= falseScore[obsOutcome];
	    }
	  }
	}
	mutL += l;
      }
      // add log(mutL) with correction
      double y = log(mutL) - sumComp;
      double t = sum + y;
      sumComp = (t - sum) - y;
      sum = t;
    }
    return sum - ((double)m) * std::log((double)infMatrix.n);
  }

  /**
   * Calculates the posterior probability of mutation l having occured first in cell k
   * \param infMatrix Influence matrix of the tree to score
   * \param postProb Array with one entry for every SNV 
   */
  void getPostProb(const InfluenceMatrix& infMatrix, double* postProb) const {
    for (unsigned i=0; i<m; ++i) { // for every SNV position
      const outcomeIndex_t * const mutRow = obsData + i * (unsigned) n;
      
      double totProb = 0;
      for (unsigned j=0; j<n; ++j) { // choose clone as origin of SNV (theta=j)
	double l = 1.;
	const bool * const infRow = infMatrix.getRow(j);

	for (unsigned k=0; k<n; ++k) { // sum over observational probability for every clone
	  bool predMutation = infRow[k];
	  outcomeIndex_t obsOutcome = mutRow[k];
	  if (predMutation) {
	    l *= trueScore[obsOutcome];
	  } else {
	    l *= falseScore[obsOutcome];
	  }
	}

	postProb[i*n+j] =  l;
	totProb += l;
      }
      // normalize probability for each mutation.
      for (unsigned j=0; j<n; ++j) {
	postProb[i*n+j] /= totProb;
      }
    }
  }
 
 private:
  /**
   * Number of observations
   */
  const unsigned m;
  /**
   * Number of non-root nodes
   */
  const nodeIndex_t n;
  /**
   * Number of possible outcomes
   */
  const outcomeIndex_t numOutcomes;
  /**
   * Mutated scores, i.e. trueScore[i] gives the likelihood to see
   * outcome $i$ if its mutated.
   */
  double * trueScore;
  /**
   * Unmutated scores, i.e. falseScore[i] gives the likelihood to see
   * outcome $i$ if its mutated.
   */
  double * falseScore;
  /**
   * Observation data, i.e. obsData[i*n+j] gives the measured SNV i
   * for the j^th non-root node
   */
  outcomeIndex_t * obsData;
};

/**
 * The TreeFinder class collects the data needed for scoring the data
 * and implementing a search algorithm.
 *
 * It collects all previous functions and can be used as a safe
 * interface.
 */
class TreeFinder {
public:
  /**
   * Creates a treeFinder object.
   *
   * It collects the scoring data and implements a searching algorithm
   * for finding the best-scoring trees.
   *
   * The arrays are copied in the construction and can therefore be
   * released afterwards.
   *
   * \param m Number of observations
   * \param n Number of non-root nodes
   * \param numOutcomes Number of outcomes in the measurement
   * \param trueScore[numOutcomes] Score for the outcomes given we predict a mutation
   * \param falseScore[numOutcomes] Score for the outcomes given we predict no mutation
   * \param obsData Observed data, i.e. obsData[i*n + j] gives the measurement
   *        for the i^th SNV at the j^th non-root node
   */
  TreeFinder(unsigned m, nodeIndex_t n, outcomeIndex_t numOutcomes,
	      const double * trueScore, const double * falseScore,
	      const outcomeIndex_t * obsData)
    : n(n),
      scoring(m, n, numOutcomes, trueScore, falseScore, obsData),
      consideredTrees(TreeCompare(n)),
      scoredTrees(ScoredTreeCompare(n)),
      waitingList(ScoredTreeCompare(n)),
      currCounter(0),
      bestCounter(0)
  {}

  /**
   * Cleans up the memory for the saved trees
   */
  ~TreeFinder() {
    TreeSet::iterator it;
    for (it = consideredTrees.begin(); it != consideredTrees.end(); it++) {
      delete [] *it;
    }
  }
  
  /**
   * Adds a tree as starting point for the searching.
   *
   * If added the used tree will be copied so that the parameter can
   * be freed afterwards.
   *
   * If the tree is invalid it will not be added and return false
   *
   * \param tree Tree saved as array of parents for non-root node
   * \return bool Whether the tree was already considered
   */
  bool addTree(const nodeIndex_t * tree) {
    if (verifyTree(n, tree)
	|| consideredTrees.find(tree) != consideredTrees.end()) {
      return false;
    } else {
      // This is a genuine new tree which we need to consider
      // Make a copy for ourselves
      nodeIndex_t * treeC = new nodeIndex_t[n];
      memcpy(treeC, tree, (unsigned)n * sizeof(nodeIndex_t));
      InfluenceMatrix infMatrix(n, treeC);
      double score = scoring.scoreTree(infMatrix);
      // Add to everything
      consideredTrees.insert(treeC);
      auto ret = scoredTrees.insert(ScoredTree(score, treeC));
      if (ret.first == scoredTrees.begin()) {
	bestCounter = currCounter;
      }
      waitingList.insert(ScoredTree(score, treeC));
      return true;
    }
  }

  /* /\** */
  /*  * Find best scoring tree by permutations */
  /*  * */
  /*  * Searches by looking at all neighbours of the so-far best scoring trees */
  /*  * Stops after given number of steps. */
  /*  *  */
  /*  * \param steps Number of times we consider all neighbours */
  /*  *\/ */
  /* std::string find(unsigned steps, unsigned delta, bool swap) { */
  /*   unsigned i=0; */
  /*   for (; i<steps && !waitingList.empty() && (currCounter-bestCounter) <= delta; ++i) { */
  /*     ++currCounter; */
  /*     ScoredTreeSet::iterator it = waitingList.begin(); */
  /*     const nodeIndex_t * tree = it->second; */
  /*     waitingList.erase(it); */
  /*     addNeighbours(tree,swap); */
  /*   } */
  /*   std::string answer; */
  /*   if (waitingList.empty()) { */
  /*     answer="All possible trees analyzed."; */
  /*   } else if (i==steps) { */
  /*     answer="Maximum number of steps reached."; */
  /*   } else if ((currCounter-bestCounter)==delta) { */
  /*     answer="Maximum number of unsuccessful steps reached."; */
  /*   } */
  /*   return answer; */
  /* } */
  
  /**
   * Find best scoring tree by permutations
   *
   * Searches by looking at all neighbours of the so-far best scoring trees
   * Stops after given number of steps.
   * 
   * \param steps Number of times we consider all neighbours
   */
  std::string find(unsigned steps, bool swap) {
    unsigned i=0;
    for (; i<steps && !waitingList.empty(); ++i) {
      ++currCounter;
      ScoredTreeSet::iterator it = waitingList.begin();
      const nodeIndex_t * tree = it->second;
      waitingList.erase(it);
      addNeighbours(tree,swap);
    }
    std::string answer;
    if (waitingList.empty()) {
      answer="All possible trees analyzed.";
    } else if (i==steps) {
      answer="Maximum number of steps reached.";
    }
    return answer;
  }

    /**
   * Find best scoring tree by permutations
   *
   * Searches by looking at all neighbours of the so-far best scoring trees
   * Stops when highest scoring tree hasn't changed for delta steps
   *
   * \param delta Number of steps after which the search is aborted if highest scoring tree hasn't changed.
   */
  std::string find2(unsigned delta, bool swap) {
    for (; !waitingList.empty() && (currCounter-bestCounter) < delta; ++currCounter) {
      ScoredTreeSet::iterator it = waitingList.begin();
      const nodeIndex_t * tree = it->second;
      waitingList.erase(it);
      addNeighbours(tree,swap);
    }
    std::string answer;
    if (waitingList.empty()) {
      answer="All possible trees analyzed.";
    } else if ((currCounter-bestCounter)==delta) {
      answer="Maximum number of unsuccessful steps reached.";
    }
    return answer;
  }

  /**
   * Returns a sorted list of all scored trees
   * \return Reference to the scored tree set
   */
  const ScoredTreeSet& getScoredTreeList() const {
    return scoredTrees;
  }

  /**
   * Returns the assumed number of non-root nodes
   * \return n
   */
  nodeIndex_t getN() const {
    return n;
  }

  /**
   * Returns the number of mutation sites in obsData
   * \return m
   */
  unsigned getM() const {
    return scoring.getM();
  }

  /**
   * Returns the total number of search steps performed.
   */
  unsigned getCurrCounter() const {
    return currCounter;
  }

  /**
   * Returns the number of the step in which the overall best Tree was found.
   */
  unsigned getBestCounter() const {
    return bestCounter;
  }

  /**
   * Scores a given tree
   * \param Tree which we want to score
   * \return score (+inf for error)
   */
  double scoreTree(const nodeIndex_t * const tree) const {
    // Verify tree
    if (verifyTree(n, tree)) {
      return std::numeric_limits<double>::infinity();
    }
    // Create influence matrix
    InfluenceMatrix infMatrix(n, tree);
    return scoring.scoreTree(infMatrix);
  }

  double scoreCluster(unsigned treeN, const nodeIndex_t * tree, 
		      const nodeIndex_t * clusterAllocation) {
    // Verify tree
    if (verifyTree(treeN, tree)) {
      return std::numeric_limits<double>::infinity();
    }
    // Create influence matrix
    InfluenceMatrix infMatrix(treeN, tree);
    return scoring.scoreCluster(infMatrix, clusterAllocation);
  }

  /**
   * Calculates posterior probability of a given tree
   * \param Tree which we want to score
   * \param Array of postProb (+inf for error) of length XXX
   * \return XXX
   */
  bool getPostProb(const nodeIndex_t * const tree, double * postProb) const {
    // Verify tree
    if (verifyTree(n, tree)) {
      return false;
    }
    // Create influence matrix
    InfluenceMatrix infMatrix(n, tree);
    scoring.getPostProb(infMatrix, postProb);
    return true;
  }

 private:
  /**
   * Adds all neighbours of a given tree
   * \param Tree of which we want to consider the neighbours
   */
  void addNeighbours(const nodeIndex_t * const tree, bool swap) {
    // Get the influence matrix
    InfluenceMatrix infMatrix(n, tree);
    // 2 Methods for finding neighbors
    // Method 1: Swap labels parent <-> node i (unless parent is root)
    // Method 2: Consider a new predecessor for the i^th non-root node

#pragma omp parallel for schedule(dynamic)
    for (int i=0; i<n; ++i) {

      if (swap) { // Method 1
	if (tree[i]!=0) { // If parent of i is not the root swap labels
	  // Okay create the new tree
	  nodeIndex_t * nTree = new nodeIndex_t[n];
	  memcpy(nTree, tree, n * sizeof(nodeIndex_t));
	  int newChild = nTree[i];
	  int newPar = i+1;
	  // swap parents of the two nodes that swap labels
	  nTree[newPar-1] = nTree[newChild-1];
	  nTree[newChild-1] = newChild; // gets relabelled to newPar in next step
	  // relabel the parents of all child nodes
	  for (int j=0; j<n; ++j) {
	    if (nTree[j]==newChild) {
	      nTree[j] = newPar; 
	    } else if (nTree[j]==newPar) {
	      nTree[j] = newChild;
	    }
	  }
	  
	  // Check if we already considered this tree
	  bool notConsidered;
#pragma omp critical (tf_considered)
	  {
	    notConsidered = (consideredTrees.find(nTree) == consideredTrees.end());
	  }
	  if (notConsidered) {
	    // We need to score the tree and add it to the lists
	    InfluenceMatrix nInfMatrix(n, nTree);
	    double score = scoring.scoreTree(nInfMatrix);
#pragma omp critical (tf_considered)
	    {
	      consideredTrees.insert(nTree);
	    }
#pragma omp critical (tf_scoredLists)
	    {
	      auto ret = scoredTrees.insert(ScoredTree(score, nTree));
	      if (ret.first == scoredTrees.begin()) {
		bestCounter = currCounter;
	      }
	      waitingList.insert(ScoredTree(score, nTree));
	    }
	  } else {
	    // The tree was already added and we can release the memory
	    delete [] nTree;
	  }
	}
      }

      // Method 2
      // Consider a new predecessor for the i^th non-root node 
      // Possible new parent
      for (int j=0; j <= n; ++j) {
	// j is a possible new parent index (including the root)
	// Check if parent is possible
	if (j==0 || !infMatrix(i,j-1)) {
	  // Okay create the new tree
	  nodeIndex_t * nTree = new nodeIndex_t[n];
	  memcpy(nTree, tree, n * sizeof(nodeIndex_t));
	  nTree[i] = j;
	  
	  // Check if we already considered this tree
	  bool notConsidered;
#pragma omp critical (tf_considered)
	  {
	    notConsidered = (consideredTrees.find(nTree) == consideredTrees.end());
	  }
	  if (notConsidered) {
	    // We need to score the tree and add it to the lists
	    InfluenceMatrix nInfMatrix(n, nTree);
	    double score = scoring.scoreTree(nInfMatrix);
#pragma omp critical (tf_considered)
	    {
	      consideredTrees.insert(nTree);
	    }
#pragma omp critical (tf_scoredLists)
	    {
	      auto ret = scoredTrees.insert(ScoredTree(score, nTree));
	      if (ret.first == scoredTrees.begin()) {
		bestCounter = currCounter;
	      }
	      waitingList.insert(ScoredTree(score, nTree));
	    }
	  } else {
	    // The tree was already added and we can release the memory
	    delete [] nTree;
	  }
	}
      }
    }
  }

  /**
   * Number of non-root nodes
   */
  const nodeIndex_t n;
  /**
   * Object for the scoring data
   */
  const ScoringData scoring;
  /**
   * Map of all considered trees.
   *
   * This is also used to keep track of the allocated memory.
   *
   * TODO Could do set using hash
   */
  TreeSet consideredTrees;
  /**
   * Map of all considered trees with scores
   *
   * Does NOT keep track of the allocated memory for the trees.
   */
  ScoredTreeSet scoredTrees;
  /**
   * Waiting list of considered trees with scores
   *
   * Does NOT keep track of the allocated memory for the trees.
   *
   * TODO Could be done using a priority_queue/heap
   */
  ScoredTreeSet waitingList;
  /**
   * Number of performed 'find' steps
   */
  unsigned int currCounter;
  /**
   * Number of 'find' step in which the best overall tree was found.
   */
  unsigned int bestCounter;
};

// Header guard close
#endif
