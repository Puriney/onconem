% Generated by roxygen2 (4.1.1): do not edit by hand
% Please edit documentation in R/oncoNEM.R
\name{getNBestTrees}
\alias{getNBestTrees}
\title{Extract best trees}
\usage{
getNBestTrees(oNEM, N)
}
\arguments{
\item{oNEM}{Object of oncoNEM class.}

\item{N}{number of trees to return.}
}
\value{
A list containing the trees and llhs.
}
\description{
Returns N best trees of oncoNEM object in vector format as well as their
respecitve log-likelihoods.
}
\note{
If N is greater than the number of trees scored so far, function returns
all trees scored so far.
}
\examples{
## simulate example data set
dat <- simulateData()
## initialize oncoNEM object
oNEM <- oncoNEM(dat$D,dat$FPR,dat$FNR)
## run initial search until best tree has not changed for 10 steps
oNEM$search(delta=10)
## print 5 best trees
getNBestTrees(oNEM,5)
}

