# Overview

OncoNEM is an R package accompanying the following publication:

Edith M Ross and Florian Markowetz, OncoNEM: Inferring tumour evolution from single-cell sequencing data, 2016, submitted.

# License

OncoNEM is licensed under the GPL v3, see the License.txt file for details.

# Installation
OncoNEM can be installed directly from Bitbucket using the devtools R package by executing the following code in an R terminal:
```
library(devtools)
install_bitbucket("edith_ross/oncoNEM")
```

# Usage
Please refer to the vignette for a toy example.